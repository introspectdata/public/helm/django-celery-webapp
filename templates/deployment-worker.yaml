apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "introspect-webapp.fullname" . }}-worker
  labels:
    app.kubernetes.io/name: {{ include "introspect-webapp.name" . }}
    helm.sh/chart: {{ include "introspect-webapp.chart" . }}
    app.kubernetes.io/instance: {{ .Release.Name }}
    app.kubernetes.io/managed-by: {{ .Release.Service }}
    app.introspectdev.io/role: worker
spec:
  replicas: {{ .Values.worker.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "introspect-webapp.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
      app.introspectdev.io/role: worker
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "introspect-webapp.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
        app.introspectdev.io/role: worker
    spec:
      containers:
        - name: {{ .Chart.Name }}-worker
          image: "{{ .Values.app.image.repository }}:{{ .Values.app.image.tag }}"
          imagePullPolicy: {{ .Values.app.image.pullPolicy }}
          cmd:
            - /bin/bash
            - -c
          args:
            - celery
            - worker
            - --app={{ .Values.app.name }}
            - --loglevel=info
            - --scheduler={{ .Values.celery.schedulerName }}
            - --pool={{ .Values.app.workerPool }}
          envFrom:
            - configMapRef:
                name: {{ template "introspect-webapp.fullname" . }}
            - secretRef:
                name: {{ template "introspect-webapp.fullname" . }}
          volumeMounts:
            - mountPath: {{ .Values.app.mediaPath }}
              name: media-path
          {{- if .Values.worker.probe }}
          livenessProbe:
            exec:
              # bash is needed to replace the environment variable
              command: [
                "/bin/bash",
                "-c",
                "celery inspect ping -A {{ .Values.app.name }} -d celery@$HOSTNAME"
              ]
            initialDelaySeconds: 30  # startup takes some time
            periodSeconds: 60  # default is quite often and celery uses a lot cpu/ram then.
            timeoutSeconds: 10  # default is too low
          readinessProbe:
            exec:
              # bash is needed to replace the environment variable
              command: [
                "/bin/bash",
                "-c",
                "celery inspect ping -A {{ .Values.app.name }} -d celery@$HOSTNAME"
              ]
            initialDelaySeconds: 30  # startup takes some time
            periodSeconds: 60  # default is quite often and celery uses a lot cpu/ram then.
            timeoutSeconds: 10  # default is too low
          {{- end }}
          resources:
            {{- toYaml .Values.worker.resources | nindent 12 }}
      {{- with .Values.worker.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.worker.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.worker.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      volumes:
        - name: media-path
        {{- if .Values.app.sharedMount.enabled }}
          persistentVolumeClaim:
            claimName: {{ if .Values.app.sharedMount.existingClaim }}{{ .Values.app.sharedMount.existingClaim }}{{- else }}{{ template "introspect-webapp.fullname" . }}{{- end }}
        {{- else }}
          emptyDir: {}
        {{- end }}
