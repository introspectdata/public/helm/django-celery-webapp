# django-celery-webapp

Helm chart for deploying our django + celery webapp stuff.. and such.

## Add the Helm Repo

IntrospectData hosts a public helm chart repository at [cloudsmith.io](https://cloudsmith.io). To add our repository, use the followiung command:

```bash
helm repo add id-public https://dl.cloudsmith.io/introspect-data/helm-public/helm/charts/
```

## Install Chart

To install the Flower Chart into your Kubernetes cluster :

```bash
export APP_NAMESPACE=<put_namespace_here>
helm upgrade -i <name> --namespace <namespace> id-public/django-celery-webapp -f <values_file_path>
```

After installation succeeds, you can get a status of Chart

```bash
helm status <name>
```

If you want to delete your Chart, use this command:

```bash
helm delete  --purge <name>
```

### Helm ingresses

The Chart provides ingress configuration to allow customization the installation by adapting the `values.yaml` depending on your setup.
Please read the comments in the `values.yaml` file for more details on how to configure your reverse proxy or load balancer.

### Chart Prefix

This Helm automatically prefixes all names using the release name to avoid collisions.

### URL prefix

This chart exposes 1 endpoint with 2 path configurations:

- application URL (with optional TLS via LetsEncrypt)
  - `/static` path served by static container
  - `/` path served by app container


## Logs

Logs for the application should be output to `stdout` which can be pulled into any logging setup of your chosing.

### Global/Shared Environment Variables (and secrets)

Environment variables are created for all values within the `envValues` and `secretValues` sections of the `vaues.yaml` file. By default, settings for Postgres, Memcache and Celery (assuming the use of separate broker and result backend) are defaulted as defined below in the default `values.yaml` file.

These are, essentially, pass-through values and the keys themselves can be whatever is mapped within the Django app settings file. An example of a set of variables that has been added to based on application needs might look like this:

```yaml
envValues:
  DATABASE_HOST: '10.24.0.3'
  DATABASE_PORT: '5432'
  DATABASE_NAME: 'webapp-db'
  DATABASE_USER: 'webapp-user'
  DATABASE_ENGINE: django.db.backends.postgresql_psycopg2
  CACHE_BACKEND: "django.core.cache.backends.memcached.MemcachedCache"
  CACHE_LOCATION: "production-memcached.production-webapp:11211"
  ALLOWED_HOSTS: "app.introspectdata.com"
  BASE_URL: "https://app.introspectdata.com"
  EMAIL_BACKEND: anymail.backends.mailgun.EmailBackend
  DEFAULT_FROM_EMAIL: app@introspectdata.com
  SERVER_EMAIL: ops@introspectdata.com
  ANYMAIL_IGNORE_RECIPIENT_STATUS: False
  ANYMAIL_MAILGUN_SENDER_DOMAIN: sandboxe524c35d5f5b4ec29b355e1bb0c9ebf5.mailgun.org
  ANYMAIL_MAILGUN_API_URL: https://api.mailgun.net/v3
  CORS_ORIGIN_WHITELIST: "https://app.introspectdata.com"
  MEDIA_ROOT: /svr/media
  DEBUG: "False"
  ALLOWED_HOSTS: "*"

secretValues:
  CELERY_RESULT_BACKEND: redis://:************@production-redis-master.production-webapp:6379/0
  CELERY_BROKER_URL: amqp://appuser:************@production-rabbitmq.production-webapp:5672//
  SECRET_KEY: ************
  DATABASE_PASSWORD: ************
  ANYMAIL_MAILGUN_API_KEY: ************
  ANYMAIL_WEBHOOK_SECRET: ************
```


### Webapp Configuration


In general, configuration for each tier of service is consistent with `helm` standards as follows with a few basic caveats:

* For components that serve traffic via tcp, `containerPort`, `service` and `ingress` are available
* The `replicaCount` for the worker_cron (celery beat) component is set to 1

```yaml
webapp:
  replicaCount: 3
  resources: {}
    # limits:
    #  cpu: 100m
    #  memory: 128Mi
    # requests:
    #  cpu: 100m
    #  memory: 128Mi
  nodeSelector: {}
  tolerations: []
  affinity: {}
  service:
    type: ClusterIP
    port: 80
  ingress:
    enabled: false
    annotations: {}
      # kubernetes.io/ingress.class: nginx
      # kubernetes.io/tls-acme: "true"
    paths: []
    hosts:
      - chart-example.local
    tls: []
    #  - secretName: chart-example-tls
    #    hosts:
    #      - chart-example.local
```

## Helm chart Configuration

The following table lists the configurable parameters of the django-celery-webapp chart and their default values.

| Parameter                                | Description                                             | Default                   |
|------------------------------------------|---------------------------------------------------------|---------------------------|
| `app.name` | Name of the app to pass to FLASK_APP and to the celery runtime | "" |
| `app.image.repository` | Name of the docker image repository to be used for deploying application-level components (django and celery) | "" |
| `app.image.tag` | Name of the docker image tag to be used for deploying application-level components (django and celery) | "latest" |
| `app.image.pullPolicy` | K8s image pull policy to use for access to this image | "IfNotPresent" |
| `app.mediaPath` | Django `MEDIA_ROOT` - used to handle shared mounts if needed | "/svr/media" |
| `app.sharedMount.enabled` | Flag determining whether or not a shared mount point will be used for this app deployment for the `MEDIA_ROOT` | "false" |
| `app.sharedMount.existingClaim` | App data Persistent Volume existing claim name, evaluated as a template | "" |
| `app.sharedMount.accessMode` | Kubernetes access mode flag for volume mount setup | "ReadWriteMany" |
| `app.sharedMount.size` | Size of the data volume | "2.5Ti" |
| `app.sharedMount.storageClass` | StorageClass of the backing PVC | "nfs" |
| `app.workerPool` | Celery worker pool type | "solo" |
| `celery.schedulerName` | Name of the scheduler to use within the Celery Beat deployment | "django_celery_beat.schedulers:DatabaseScheduler" |
| `celery.beatPidPath` | Path of the pid Celery creates when running the Beat scheduler - used for healthchecks in k8s | "celery.pid" |


Full and up-to-date documentation can be found in the comments of the `values.yaml` file.
